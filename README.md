
Project to test bots to scan Android apps on a grand scale.  To use this on your
fork, you need to have Maintainer access to this project.  The _issuebot_ fork
that it runs is set by the env var `ISSUEBOT_FORK` which is set in (careful, the
username is case-sensitive!).  This will run the _master_ branch of that fork.

* <a href="https://gitlab.com/trackingthetrackers/rfp/-/settings/ci_cd#js-cicd-variables-settings">Settings -> CI/CD -> Variables</a>
